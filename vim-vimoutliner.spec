%define otl2html_version 1.40
%global upstream_name vimoutliner

Name:           vim-%{upstream_name}
Version:        0.4.0
Release:        8%{?dist}
Summary:        Script for building an outline editor on top of Vim
# LICENSE says v2, but the code all says v2+
License:        GPLv2+
URL:            https://github.com/vimoutliner/vimoutliner
Source0: https://github.com/vimoutliner/vimoutliner/archive/%{version}/%{upstream_name}-%{version}.tar.gz
# FIXME: www.vimoutliner.org doesn't exist anymore
# the following two tarballs are from http://www.vimoutliner.org as well, but
# they are in CMS without good URLs.
Source1:        otl2html-%{otl2html_version}.tgz
Source2:        otl_handler.tgz
# things which are in discussion about putting upstream
Source3:        vimoutliner-local.tar.bz2
Source4:        vimoutliner-README.Fedora
Source5:        vimoutliner-Makefile
Source6:        vim-vimoutliner.metainfo.xml
BuildArch:      noarch
BuildRequires:      perl-generators
Requires:       vim-enhanced perl-interpreter python2
Requires(post): vim-enhanced
Requires(postun): vim-enhanced
#Requires:       libxml-writer-perl libpalm-perl

%description
Vimoutliner provides commands for building using the Vim text editor as an
outline editor. For more explanation on what outlines are and what they are
good for see the script's webpage at
https://github.com/vimoutliner/vimoutliner and the general discussion of outlines on
http://www.troubleshooters.com/tpromag/199911/199911.htm.

%prep
%setup -q -n %{upstream_name}-%{version} -a 1 -a 2 -a 3
cp -f -p  %{SOURCE4} README.Fedora
cp -f -p  %{SOURCE5} Makefile
# mv ../{CHANGELOG,INSTALL,README*,install.sh,LICENSE} .
find . -name .\*.swp -delete
mv otl2html-* doc/otl2html
mv otl_handler doc/otl_handler
mv doc/otl2html/README README.otl2html
mv doc/otl_handler/README README.otl_handler
find doc/ -type f -perm /111 -exec chmod -x '{}' \;
rm doc/otl2html/otl2html.py

%build
cp -r doc examples

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
install -m a+rx,u+w -d $RPM_BUILD_ROOT%{_sysconfdir}
install -p -m a+r,u+w vimoutlinerrc \
   $RPM_BUILD_ROOT%{_sysconfdir}/vimoutlinerrc
install -m a+rx,u+w -d $RPM_BUILD_ROOT%{_datadir}/mime/packages
install -p -m a+r,u+w vimoutliner-mimefile.xml \
   $RPM_BUILD_ROOT%{_datadir}/mime/packages/vimoutliner.xml

# install app metadata
install -p -D %{SOURCE6} %{buildroot}%{_datadir}/appdata/%{name}.metainfo.xml

%post
touch --no-create %{_datadir}/mime/packages &> /dev/null || :
cd %{_datadir}/vim/vimfiles/doc
vim -u NONE -esX -c "helptags ." -c quit
grep -q "filetype plugin on" /etc/vimrc \
    || echo -e "\nfiletype plugin on" >>/etc/vimrc

%postun
if [ $1 -eq 0 ]; then
   touch --no-create %{_datadir}/mime/packages &> /dev/null || :
   update-mime-database %{?fedora:-n} %{_datadir}/mime &> /dev/null || :
   cd %{_datadir}/vim/vimfiles/doc
   vim -u NONE -esX -c "helptags ." -c quit
fi

%posttrans
update-mime-database %{?fedora:-n} %{_datadir}/mime &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc README.otl2html README.otl_handler README.Fedora LICENSE TODO.otl
%doc examples CHANGELOG
%config(noreplace) %{_sysconfdir}/vimoutlinerrc
#%_{_bindir}/otl2html
%{_bindir}/votl_maketags
%{_datadir}/vim/vimfiles/*/*
%{_datadir}/mime/packages/vimoutliner.xml
%{_mandir}/man1/*.1.gz
%{_datadir}/appdata/*.xml

%changelog
* Fri Feb 16 2018 Matěj Cepl <mcepl@redhat.com> - 0.4.0-8
- Add AppStream metadata
  (https://www.freedesktop.org/wiki/Distributions/AppStream/, fixes #1128821)

* Fri Feb 16 2018 Matěj Cepl <mcepl@redhat.com> - 0.4.0-7
- Remove Group: metadata, it is really unnecessary.

* Fri Feb 09 2018 Iryna Shcherbina <ishcherb@redhat.com> - 0.4.0-6
- Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.4.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.4.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jul 13 2017 Petr Pisar <ppisar@redhat.com> - 0.4.0-3
- perl dependency renamed to perl-interpreter
  <https://fedoraproject.org/wiki/Changes/perl_Package_to_Install_Core_Modules>

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.4.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Wed Nov 23 2016 Petr Lautrbach <plautrba@redhat.com> - 0.4.0-1
- Update to 0.4.0 release

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.3.7-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.7-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Fri Dec 05 2014 Matej Cepl <mcepl@redhat.com> - 0.3.7-6
- Reverse intermediate commits which broke the build.

* Thu Oct 02 2014 Rex Dieter <rdieter@fedoraproject.org> 0.3.7-5
- update mime scriptlets

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.7-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Apr 06 2014 Matěj Cepl <mcepl@redhat.com> - 0.3.7-3
- Update to the upstream release.

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.6-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Jul 18 2013 Petr Pisar <ppisar@redhat.com> - 0.3.6-4
- Perl 5.18 rebuild

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri May 18 2012 Matěj Cepl <mcepl@redhat.com> - 0.3.6-1
- New upstream and new upstream version.

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-15
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Jan 06 2010 Matěj Cepl <mcepl@redhat.com> - 0.3.4-13
- Fix vimoutliner-mc-build.patch not to set maplocalleader

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Sep  8 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.3.4-10
- fix license tag

* Tue Apr  3 2007 Matej Cepl <mcepl@redhat.com> - 0.3.4-9
- changed BuildRoot to more sane value, now it is allowed.

* Mon Jan  8 2007 Matěj Cepl <mcepl@redhat.com> 0.3.4-8
- changed named to vim-vimoutliner to follow Fedora Naming
  Guidelines.
* Mon Dec 11 2006 Matěj Cepl <mcepl@redhat.com> 0.3.4-7
- package should own only files in /usr/share/vim/vimfiles/*/
  directories, not the directory itself. Unfortunately, it is bad
  hack, because /usr/share/vim/vimfiles directory itself is not
  owned by any package (see bug no. 219154).
- made comments about origin of additional Sources.
- cp gets additional -p
- calling of update-desktop-database removed
- removed helpztags
* Mon Dec  4 2006 Matěj Cepl <mcepl@redhat.com> 0.3.4-6
- other ugly Debianism: don't put additional files into diff,
  when we could have another source file.
- again fixed problem with levels starting with non-ASCII
  character
* Sat Dec  2 2006 Matěj Cepl <mcepl@redhat.com> 0.3.4-5
- No macros in changelog anymore.
- Also there are no any <TAB>s in this file.
- Package doesn't own directories in %%files when not required.
- Sources have URLs when possible.
* Tue Nov 28 2006 Matěj Cepl <mcepl@redhat.com> 0.3.4-4
- Created Makefile for installation (so we could offer it upstream).
- compression of manpages is done automagically by rpmbuild, no need
  to do it ourselves.
* Sat Nov 11 2006 Matěj Cepl <mcepl@redhat.com> 0.3.4-3
- Finished Fedorization of originally README.Debian
- added otl2html and otl_handler to examples/ subdirectory of
  %%doc.
- fixed manpages (don't install manpages for scripts we don't
  carry).
* Wed Nov 8 2006 Matěj Cepl <mcepl@redhat.com> 0.3.4-2
- Fixed paths in ftplugin/vo_base.vim so that additional plugins
  actually load when requested.
* Tue Nov 7 2006 Matěj Cepl <mcepl@redhat.com> 0.3.4-1
- Initial build.
